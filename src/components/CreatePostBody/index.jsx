import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { addPost } from "../redux/post";
import { connect } from "react-redux";

import { bindActionCreators } from "redux";
import { SmileOutlined } from "@ant-design/icons";

function CreatePostBody({ placeholder, icon, onTextChange, addPost }) {
  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => addPost(data.text);
  const [textValue, setTextValue] = useState("");
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="row px-1">
        <div className="col-3">
          <SmileOutlined className="smile-icon" />
        </div>
        <div className="col-9">
          <textarea
            name="text"
            value={textValue}
            onChange={(e) => setTextValue(e.target.value)}
            className="post-textarea"
            ref={register({ required: true, maxLength: 20 })}
          />
        </div>
        <div className="col-12">
          <button
            type="submit"
            disabled={textValue.length === 0}
            className="post-btn"
          >
            پست
          </button>
        </div>
      </div>
    </form>
  );
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ addPost: addPost }, dispatch);
};
export default connect(null, mapDispatchToProps)(CreatePostBody);
