import React, { useState } from "react";
import { Modal } from "antd";
import { connect } from "react-redux";
import { PictureOutlined, TagOutlined } from "@ant-design/icons";
import PostCard from "./PostCard";

const AddPostCard = ({ newPost}) => {
 

  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const showModal = () => {
    setVisible(true);
  };
  const handleOk = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setVisible(false);
    }, 2000);
  };
  const handleCancel = () => {
    setVisible(false);
  };
  console.log(newPost)

  return (
    <div className="row justify-content-center mt-2">
      <div className="col-5">
        <div className="row">
          <div className="col-12 add-post-card">
            <div className="row">
              <div className="add-post-card__image">
                <img src="/assets/images/profile1.jpg" alt="profile" />
              </div>
              <div className="add-post-card__btn" onClick={showModal}>
                در ذهنت چه می گذرد؟
              </div>
              <Modal
                visible={visible}
                title="ایجاد پست"
                onOk={handleOk}
                onCancel={handleCancel}
                closable={false}
                footer={[
                  <button
                    key="upload"
                    className="upload-btn"
                    onClick={handleOk}
                  >
                    <PictureOutlined />
                    <span>عکس/ویدیو</span>
                  </button>,
                  <button
                    key="tag"
                    loading={loading}
                    onClick={handleOk}
                    className="upload-btn"
                  >
                    <TagOutlined />
                    <span>برچسب گذاری ...</span>
                  </button>,
                ]}
              >
               
              </Modal>
            </div>
          </div>
          <PostCard />
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    newPost: state.newPost,
  };
};

export default connect(mapStateToProps)(AddPostCard);
